# Shinesoftware ElasticSearch Fix

we have found some issue when Elasticsearch is enabled using Magento >= 2.3.5. For more details look at https://github.com/magento/magento2/issues/27112

The error message is this:

``Fatal error: Uncaught TypeError: Argument 1 passed to Magento\Elasticsearch\Model\Adapter\FieldMapper\Product\AttributeProvider::getByAttributeCode() must be of the type string``

## Install by Composer

    ``shinesoftware/elasticfix``

 - [Main Functionalities](#markdown-header-main-functionalities)
 - [Installation](#markdown-header-installation)
 - [Configuration](#markdown-header-configuration)
 - [Specifications](#markdown-header-specifications)
 - [Attributes](#markdown-header-attributes)


## Main Functionalities
List of fixes for Magento

## Installation
\* = in production please use the `--keep-generated` option

## Configuration
No need any configuration

## Fix Specifications

- ElasticSearch MG 234 #27112 (https://github.com/magento/magento2/issues/27112)



