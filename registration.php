<?php
/**
 * Copyright © Shine Software Italy All rights reserved.
 * See COPYING.txt for license details.
 */
use Magento\Framework\Component\ComponentRegistrar;

ComponentRegistrar::register(ComponentRegistrar::MODULE, 'Shinesoftware_ElasticFix', __DIR__);

